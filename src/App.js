//RESOURCES USED
//watched Kano's demo
//https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/React_interactivity_events_state
//https://www.npmjs.com/package/uuid
//pulled handleDelete off of assessment instructions

import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleTodoInput = (e) => {
    //pulled from reactjs docs
    this.setState({ value: e.target.value });
  }

  handleSubmit = (e) => {
    //pulled off of reactjs docs
    if(e.key === "Enter") {
      this.handleTodoAdd()
    }

  }

  handleTodoAdd = () => {
    //need to make a copy of what we have
    const newTodo = {
      //pulled out of json file and used uuid for unlimited random numbers
      "userId": 1,
      "id": uuidv4(),
      "title": this.state.value,
      "completed": false 
  }
  //should give a list of all the todos
  //code from kano
  //spread. gt list of all todos currently in state
  const newTodos = [...this.state.todos, newTodo]
  this.setState({ 
    todos: newTodos, 
    value: ""
  })
}



  //grabbed from assessment
  //didn't need to hand in event at all
  //deleting todos by x'ing them out
  //the naming on these is a little confusing
  handleDelete = (todoId) => {
    /* const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.id !== todoId
    );
    this.setState({ todos: newTodos }); */
    const { todos } = this.state;
    this.setState({
      todos: todos.filter(todo => todo.id !== todoId)
    });
  //};
  };

//code from Kano's demo. NNaming these was super confusing for me
  handleCheck = (checkId) => {
    //map makes a copy of the array
    /* const newTodos = this.state.todos.map((todoItem) => {
      if (todoItem.id === checkId) {
        todoItem.completed = !todoItem.completed;
      }
      return todoItem;
    });
    this.setState({ todos: newTodos }); 
    //user can mark as complete when click on the circle
    
  };  */
  const { todos } = this.state;
    this.setState({
      todos: todos.map(
        todo =>
          todo.id === checkId
            ? {
                ...todo,
                completed: !todo.completed
              }
            : todo
      )
    });
  };


  handleCompletedDelete = () => {
    /*const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.completed !== true
    );
    this.setState({ todos: newTodos }); */
    const { todos } = this.state;
    this.setState({
      todos: todos.filter(todo => todo.completed === false)
    });
  //};
  };


  render() {
    return (
      <React.Fragment>
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            type="text"
            onChange={this.handleTodoInput}
            onKeyDown={this.handleSubmit}
            value={this.state.value}
            className="new-todo"
            onChange={this.handleTodoInput}
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>

        <TodoList
          todos={this.state.todos}
          handleCheck={this.handleCheck}
          handleDelete={this.handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
            onClick={this.handleCompletedDelete}
            className="clear-completed">
            Clear completed
          </button>
        </footer>
      </section>
      </React.Fragment>
    );
  }
}
  

//initially isn't even called in App class
//it's called in TodoList and TodoList is called in class
//spent hours looking for syntax errors. fun

class TodoItem extends Component {
  render() {
    return (
      <React.Fragment>
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onChange={() => this.props.handleCheck(this.props.id)}
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            //this.props is an object that holds everything that you hand in inside TodoItem in class TodoList
            
            onClick={() => this.props.handleDelete(this.props.id)}
          />
        </div>
      </li>
      </React.Fragment>
    );
  }
}



//called in App class
//map each one of these out to a map todo item
//props is just an object that has keys
//title, id and completed are just variables
class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              handleCheck={this.props.handleCheck}
              handleDelete={this.props.handleDelete}
              title={todo.title}
              id={todo.id}
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}


export default App;
